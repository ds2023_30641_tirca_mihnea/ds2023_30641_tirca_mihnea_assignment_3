package ro.sd.usermanagement.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {
    @Value("${device_management.host}")
    private String host;

    @Value("${device_management.port}")
    private String port;

    @Bean
    public WebClient deviceWebClient() {
        return WebClient.create("http://" + host + ":" + port);
    }
}
