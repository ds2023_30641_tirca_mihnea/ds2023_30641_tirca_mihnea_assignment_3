package ro.sd.usermanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ro.sd.usermanagement.model.entity.User;
import ro.sd.usermanagement.model.enums.UserRole;
import ro.sd.usermanagement.repository.UserRepository;

import java.util.Optional;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public CommandLineAppStartupRunner(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        Optional<User> optionalAdmin = userRepository.findByUsername("admin");
        if(optionalAdmin.isEmpty()) {
            User admin = User.builder()
                    .username("admin")
                    .password(passwordEncoder.encode("admin"))
                    .firstName("Administrator")
                    .lastName("Administrator")
                    .role(UserRole.ADMIN)
                    .build();
            userRepository.save(admin);
        }
        
        Optional<User> optionalClient1 = userRepository.findByUsername("client1");
        if(optionalClient1.isEmpty()) {
            User client1 = User.builder()
                    .username("client1")
                    .password(passwordEncoder.encode("client1"))
                    .firstName("Mihnea")
                    .lastName("Tirca")
                    .role(UserRole.CLIENT)
                    .build();
            userRepository.save(client1);
        }

        Optional<User> optionalClient2 = userRepository.findByUsername("client2");
        if(optionalClient2.isEmpty()) {
            User client2 = User.builder()
                    .username("client2")
                    .password(passwordEncoder.encode("client2"))
                    .firstName("Rares")
                    .lastName("Tirca")
                    .role(UserRole.CLIENT)
                    .build();
            userRepository.save(client2);
        }
    }
}
