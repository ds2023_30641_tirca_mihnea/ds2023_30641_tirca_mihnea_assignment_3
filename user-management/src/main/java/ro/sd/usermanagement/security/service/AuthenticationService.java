package ro.sd.usermanagement.security.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ro.sd.usermanagement.model.entity.User;
import ro.sd.usermanagement.repository.UserRepository;
import ro.sd.usermanagement.security.model.dto.AuthenticationRequest;
import ro.sd.usermanagement.security.model.dto.AuthenticationResponse;
import ro.sd.usermanagement.security.model.dto.RegisterRequest;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse register(RegisterRequest request) {
        User user = RegisterRequest.toEntity(request, passwordEncoder);

        userRepository.save(user);

        String jwt = jwtService.generateToken(user);
        return new AuthenticationResponse(jwt);
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        String username = request.username();
        String password = request.password();

        authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(
                  username,
                  password
          )
        );

        User user = userRepository.findByUsername(request.username())
                .orElseThrow(() -> new UsernameNotFoundException("User with username " + username + " not found!"));

        String jwt = jwtService.generateToken(user);
        return new AuthenticationResponse(jwt);
    }
}
