package ro.sd.usermanagement.security.model.dto;

import org.springframework.security.crypto.password.PasswordEncoder;
import ro.sd.usermanagement.model.entity.User;
import ro.sd.usermanagement.model.enums.UserRole;

public record RegisterRequest(String firstName, String lastName, String username, String password) {
    public static User toEntity(RegisterRequest dto, PasswordEncoder passwordEncoder) {
        return User.builder()
                .firstName(dto.firstName())
                .lastName(dto.lastName())
                .username(dto.username())
                .password(passwordEncoder.encode(dto.password()))
                //todo
                .role(UserRole.CLIENT)
                .build();
    }
}
