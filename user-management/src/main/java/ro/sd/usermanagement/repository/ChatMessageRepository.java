package ro.sd.usermanagement.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ro.sd.usermanagement.model.entity.ChatMessage;
import ro.sd.usermanagement.model.entity.User;

import java.util.List;
import java.util.UUID;

@Repository
public interface ChatMessageRepository extends CrudRepository<ChatMessage, UUID> {

    @Query("""
    select m
    from ChatMessage m
    where m.sender = :user or m.receiver = :user
    order by max(m.timestamp) desc
""")
    List<ChatMessage> findAllByUserOrdered(User user);
}
