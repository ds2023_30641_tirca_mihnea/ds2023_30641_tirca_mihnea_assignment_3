package ro.sd.usermanagement.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import ro.sd.usermanagement.controller.exceptions.DeviceMicroserviceCommunicationException;
import ro.sd.usermanagement.model.dto.MyDeviceInfoDto;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class RestService {
    private final WebClient deviceWebClient;

    @Autowired
    public RestService(WebClient deviceWebClient) {
        this.deviceWebClient = deviceWebClient;
    }

    public List<MyDeviceInfoDto> getAllDevicesByUserId(UUID userId) {
        ResponseEntity<List<MyDeviceInfoDto>> response = deviceWebClient
                .get()
                .uri("/api/devices/userId/{userId}", userId)
                .retrieve()
                .toEntity(new ParameterizedTypeReference<List<MyDeviceInfoDto>>() {
                })
                .block();

        if (response != null && response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }
        throw new DeviceMicroserviceCommunicationException();
    }
}
