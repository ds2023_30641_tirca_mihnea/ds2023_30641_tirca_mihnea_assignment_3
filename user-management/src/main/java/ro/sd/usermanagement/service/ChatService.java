package ro.sd.usermanagement.service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.sd.usermanagement.controller.exceptions.UserNotFoundException;
import ro.sd.usermanagement.model.dto.ChatDto;
import ro.sd.usermanagement.model.dto.ChatMessageDto;
import ro.sd.usermanagement.model.entity.ChatMessage;
import ro.sd.usermanagement.model.entity.User;
import ro.sd.usermanagement.repository.ChatMessageRepository;
import ro.sd.usermanagement.repository.UserRepository;
import ro.sd.usermanagement.security.service.JwtService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class ChatService {
    private ChatMessageRepository messageRepository;

    private UserRepository userRepository;

    private final JwtService jwtService;

    private final SimpMessagingTemplate template;

    @Autowired
    public ChatService(ChatMessageRepository messageRepository, UserRepository userRepository, JwtService jwtService, SimpMessagingTemplate template) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
        this.jwtService = jwtService;
        this.template = template;
    }

    public List<ChatDto> getMyChats(String token) {
        String username = jwtService.extractUsernameFromBearerToken(token);
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        return getChatsByUser(user);
    }

    private List<ChatDto> getChatsByUser(User user) {
        List<ChatMessage> chatMessages = messageRepository.findAllByUserOrdered(user);
        List<ChatMessageDto> messages = chatMessages.stream().map(ChatMessageDto::fromEntity).toList();
        return groupMessagesIntoChats(messages);
    }

    private static List<ChatDto> groupMessagesIntoChats(List<ChatMessageDto> messages) {
        Map<List<String>, List<ChatMessageDto>> groupedMessages = messages.stream()
                .collect(Collectors.groupingBy(
                        messageDto -> Arrays.asList(messageDto.sender(), messageDto.receiver())
                ));

        return groupedMessages.entrySet().stream()
                .map(entry -> new ChatDto(entry.getKey().get(0), entry.getKey().get(1), entry.getValue()))
                .collect(Collectors.toList());
    }

    public ChatMessageDto receiveMessage(ChatMessageDto message) {
        template.convertAndSendToUser(message.receiver(), "/private", message);

        return message;
    }

    public List<String> getMyContacts(String token) {
        String username = jwtService.extractUsernameFromBearerToken(token);
        return userRepository.getContacts(username);
    }
}
