package ro.sd.usermanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ro.sd.usermanagement.model.dto.ChatDto;
import ro.sd.usermanagement.model.dto.ChatMessageDto;
import ro.sd.usermanagement.service.ChatService;

import java.util.List;

@RestController
@RequestMapping("/api/chat")
public class ChatController {
    private final ChatService chatService;

    @Autowired
    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_ADMIN')")
    @GetMapping
    public List<ChatDto> getMyChats(@RequestHeader(name = "Authorization") String token) {
        return chatService.getMyChats(token);
    }

    @PreAuthorize("hasAnyRole('ROLE_CLIENT', 'ROLE_ADMIN')")
    @GetMapping("/users")
    public List<String> getMyContacts(@RequestHeader(name = "Authorization") String token) {
        return chatService.getMyContacts(token);
    }

    @MessageMapping("/chat")
    public ChatMessageDto receiveMessage(@Payload ChatMessageDto message) {
        return chatService.receiveMessage(message);
    }
}
