package ro.sd.usermanagement.controller.exceptions;

import ro.sd.usermanagement.model.entity.User;

public class UserNotFoundException extends EntryNotFoundException {
    public UserNotFoundException(String username) {
        super(User.class);
        this.getBody()
                .setDetail("Entry of class " + User.class.getSimpleName() + "with username " + username + " not found");
    }
}
