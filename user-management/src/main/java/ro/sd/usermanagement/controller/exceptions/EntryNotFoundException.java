package ro.sd.usermanagement.controller.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.ErrorResponseException;

import java.util.UUID;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.ProblemDetail.forStatusAndDetail;

public class EntryNotFoundException extends ErrorResponseException {
    public EntryNotFoundException() {
        super(
                NOT_FOUND,
                forStatusAndDetail(
                        HttpStatus.NOT_FOUND,
                        "Entry not found"
                ),
                null
        );
    }

    public EntryNotFoundException(Class<?> c){
        super(
                NOT_FOUND,
                forStatusAndDetail(
                        HttpStatus.NOT_FOUND,
                        "Entry of class " + c.getSimpleName() + " not found"
                ),
                null
        );
    }

    public EntryNotFoundException(Class<?> c, UUID id) {
        super(
                NOT_FOUND,
                forStatusAndDetail(
                        HttpStatus.NOT_FOUND,
                        "Entry of class " + c.getSimpleName() + " with id " + id + " not found"
                ),
                null
        );
    }
}
