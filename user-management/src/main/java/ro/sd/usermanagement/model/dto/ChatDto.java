package ro.sd.usermanagement.model.dto;

import lombok.Builder;

import java.util.List;

/**
 * Represents the dto of a single chat between two users
 * Used when displaying the chat in frontend
 * @param user1 One user's username
 * @param user2 Other user's username
 * @param messages
 */
@Builder
public record ChatDto(String user1, String user2, List<ChatMessageDto> messages) {
}

