package ro.sd.usermanagement.model.dto;

import ro.sd.usermanagement.model.enums.DeviceStatus;

/**
 * Dto displayed on client's "My Devices" page
 */
public record MyDeviceInfoDto(String name, DeviceStatus status, double maxConsumption) {
}
