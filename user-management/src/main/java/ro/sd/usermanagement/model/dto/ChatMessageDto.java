package ro.sd.usermanagement.model.dto;

import ro.sd.usermanagement.model.entity.ChatMessage;

public record ChatMessageDto(String sender, String receiver, String text, long timestamp) {
    public static ChatMessageDto fromEntity(ChatMessage message) {
        return new ChatMessageDto(
                message.getSender().getUsername(),
                message.getReceiver().getUsername(),
                message.getText(),
                message.getTimestamp()
        );
    }
}
