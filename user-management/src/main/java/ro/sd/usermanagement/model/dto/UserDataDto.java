package ro.sd.usermanagement.model.dto;

import lombok.Builder;
import ro.sd.usermanagement.model.entity.User;
import ro.sd.usermanagement.model.enums.UserRole;

import java.util.UUID;

/**
 * Data about the currently logged-in user, stored in Redux
 */
@Builder
public record UserDataDto(
        UUID id,
        String username,
        String firstName,
        String lastName,
        UserRole role
) {
    public static UserDataDto fromEntity(User user) {
        return UserDataDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role(user.getRole())
                .build();
    }
}
