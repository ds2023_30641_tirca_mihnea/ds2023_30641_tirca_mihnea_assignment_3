package ro.sd.usermanagement.model.dto;

import lombok.Builder;
import ro.sd.usermanagement.model.entity.User;
import ro.sd.usermanagement.model.enums.UserRole;

/**
 * User dto used in the request body of the 'update user' endpoint
 */
@Builder
public record UserUpdateDto(String username, String firstName, String lastName, UserRole role) {
    public void update(User user) {
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setRole(role);
    }
}
