package ro.sd.devicemanagement.model.dto;

import lombok.Builder;
import ro.sd.devicemanagement.model.entity.Device;
import ro.sd.devicemanagement.model.enums.DeviceStatus;

@Builder
public record MyDeviceInfoDto(String name, DeviceStatus status, double maxConsumption) {
    public static MyDeviceInfoDto fromEntity(Device device) {
        return MyDeviceInfoDto.builder()
                .name(device.getName())
                .status(device.getStatus())
                .maxConsumption(device.getMaxConsumption())
                .build();
    }
}
