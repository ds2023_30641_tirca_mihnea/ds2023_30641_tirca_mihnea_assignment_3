package ro.sd.devicemanagement.model.enums;

public enum DeviceChangeType {
    CREATE,
    UPDATE,
    DELETE
}
