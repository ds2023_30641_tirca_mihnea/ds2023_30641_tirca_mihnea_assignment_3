package ro.sd.devicemanagement.model.dto;

import ro.sd.devicemanagement.model.entity.Device;
import ro.sd.devicemanagement.model.enums.DeviceStatus;

import java.util.UUID;

/**
 * Information filled by an admin when updating a device
 */
public record DeviceUpdateDto(String name, DeviceStatus status, double maxConsumption, UUID userId) {
    public void update(Device device) {
        device.setName(name);
        device.setStatus(status);
        device.setMaxConsumption(maxConsumption);
        device.setUserId(userId);
    }
}
