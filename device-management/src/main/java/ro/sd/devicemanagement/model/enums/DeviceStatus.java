package ro.sd.devicemanagement.model.enums;

public enum DeviceStatus {
    ONLINE,
    OFFLINE
}
