package ro.sd.devicemanagement.model.dto;

import lombok.Builder;
import ro.sd.devicemanagement.model.entity.Device;
import ro.sd.devicemanagement.model.enums.DeviceStatus;

import java.util.UUID;

/**
 * Device information displayed on the admin's device list page
 */
@Builder
public record DeviceInfoDto(UUID id, String name, DeviceStatus status, double maxConsumption, UUID userId) {
    public static DeviceInfoDto fromEntity(Device device) {
        return DeviceInfoDto.builder()
                .id(device.getId())
                .name(device.getName())
                .status(device.getStatus())
                .maxConsumption(device.getMaxConsumption())
                .userId(device.getUserId())
                .build();
    }
}
