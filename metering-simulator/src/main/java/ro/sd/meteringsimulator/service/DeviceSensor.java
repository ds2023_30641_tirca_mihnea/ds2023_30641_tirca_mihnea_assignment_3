package ro.sd.meteringsimulator.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ro.sd.meteringsimulator.dto.SensorData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Instant;
import java.util.UUID;
import java.util.List;
import java.util.ArrayList;

/**
 * Message Producer
 */
@Service
public class DeviceSensor {
    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routing_key.sensor}")
    private String routingKeySensor;

    private final RabbitTemplate rabbitTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceSensor.class);

    private int currentReadingIndex = 0;
    private final List<Double> readings;

    @Value("${deviceId}")
    private UUID deviceId;

    @Autowired
    public DeviceSensor(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
        this.readings = new ArrayList<>();
        loadReadingsFromCsv();
    }

    // reads sensor each second, instead of each 10 minutes
    @Scheduled(initialDelay = 1000, fixedDelay = 1000)
    public void sendMessage() {
        SensorData sensorData = new SensorData(
                Instant.now().getEpochSecond(),
                deviceId,
                getCurrentSensorReading()
        );
        rabbitTemplate.convertAndSend(exchange, routingKeySensor, sensorData);
        LOGGER.info("Message sent -> " + sensorData);
    }
    private void loadReadingsFromCsv() {
        try {
            ClassPathResource resource = new ClassPathResource("sensor.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                if (StringUtils.hasText(line)) {
                    readings.add(Double.parseDouble(line.trim()));
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error loading readings from CSV", e);
        }
    }

    private double getCurrentSensorReading() {
        if (readings.isEmpty()) {
            LOGGER.warn("No readings available in the CSV file.");
            return 0.0;
        }
        double currentReading = readings.get(currentReadingIndex);
        currentReadingIndex = (currentReadingIndex + 1) % readings.size();

        return currentReading;
    }
}
