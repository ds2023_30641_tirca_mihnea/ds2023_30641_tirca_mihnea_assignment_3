package ro.sd.moncom.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ro.sd.moncom.model.entity.Device;

@Service
public class Notifier {
    private static final Logger LOGGER = LoggerFactory.getLogger(Notifier.class);

    //todo
    public void notifyUser(Device device) {
        LOGGER.info("Device " + device.getName() + " exceeded maximum consumption limit!");
    }
}
