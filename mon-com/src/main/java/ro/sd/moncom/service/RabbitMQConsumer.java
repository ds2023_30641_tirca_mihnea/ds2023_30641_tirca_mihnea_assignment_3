package ro.sd.moncom.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.moncom.dto.DeviceChangeDto;
import ro.sd.moncom.dto.SensorData;

import java.util.UUID;

@Service
public class RabbitMQConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQConsumer.class);
    private final DeviceService deviceService;

    @Autowired
    public RabbitMQConsumer(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @RabbitListener(queues = {"${rabbitmq.queue.sensor}"})
    public void consumeSensorData(SensorData sensorData) {
        LOGGER.info("Received sensor data -> " + sensorData);
        deviceService.saveSensorData(sensorData);
    }

    @RabbitListener(queues = {"${rabbitmq.queue.change}"})
    public void consumeDeviceChange(DeviceChangeDto changeDto) {
        LOGGER.info("Received device " + changeDto.type().toString() + " request -> " + changeDto);
        deviceService.change(changeDto);
    }
}
