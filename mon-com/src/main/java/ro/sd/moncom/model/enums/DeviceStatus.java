package ro.sd.moncom.model.enums;

public enum DeviceStatus {
    ONLINE,
    OFFLINE
}
