import {GridRowModes, GridRowModesModel, GridRowsProp, GridToolbarContainer, GridValidRowModel} from "@mui/x-data-grid";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import * as React from "react";

interface EditToolbarProps {
    setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void;
    setRowModesModel: (
        newModel: (oldModel: GridRowModesModel) => GridRowModesModel,
    ) => void;
    defaultNewRow?: GridValidRowModel;
}

function EditToolbar(props: EditToolbarProps) {
    const {setRows, setRowModesModel, defaultNewRow} = props;

    const handleClick = () => {
        if(defaultNewRow !== undefined) {
            setRows((oldRows) => [...oldRows, defaultNewRow]);
            setRowModesModel((oldModel) => ({
                ...oldModel,
                [defaultNewRow.id]: {mode: GridRowModes.Edit, fieldToFocus: 'name'},
            }));
        }
    };

    return (
        <GridToolbarContainer>
            <Button color="primary" startIcon={<AddIcon/>} onClick={handleClick} disabled={defaultNewRow === undefined}>
                Add record
            </Button>
        </GridToolbarContainer>
    );
}

export default EditToolbar;