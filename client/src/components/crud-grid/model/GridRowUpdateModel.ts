import {GridRowModel} from "@mui/x-data-grid";

type GridRowUpdateModel = Omit<GridRowModel, 'id'>;

export type {GridRowUpdateModel}