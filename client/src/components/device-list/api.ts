import {deviceManagementAxios} from "../../axios/axios";
import DeviceInfo from "./model/DeviceInfo";
import {DEVICE_API} from "../../model/constants";
import DeviceUpdate from "./model/DeviceUpdate";
import DeviceCreation from "./model/DeviceCreation";
import {AxiosResponse} from "axios";

export const getAllDevices = async () => {
    return await deviceManagementAxios.get<DeviceInfo[]>(`${DEVICE_API}`);
}

export const updateDevice = async (deviceId: string, deviceUpdate: DeviceUpdate) => {
    return await deviceManagementAxios.put<DeviceInfo>(
        `${DEVICE_API}/${deviceId}`,
        deviceUpdate
    );
}

export const deleteDevice = async (deviceId: string) => {
    return await deviceManagementAxios.delete<void>(`${DEVICE_API}/${deviceId}`);
}

export const createDevice = async (deviceCreation: DeviceCreation): Promise<AxiosResponse<DeviceInfo>> => {
    return await deviceManagementAxios.post<DeviceInfo>(`${DEVICE_API}`, deviceCreation);
}