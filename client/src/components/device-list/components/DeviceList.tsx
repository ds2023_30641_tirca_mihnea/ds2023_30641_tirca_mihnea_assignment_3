import {GridColDef, GridValidRowModel} from "@mui/x-data-grid";
import DeviceInfo from "../model/DeviceInfo";
import {useEffect, useState} from "react";
import {createDevice, deleteDevice, getAllDevices, updateDevice} from "../api";
import CrudGrid from "../../crud-grid/components/CrudGrid";
import {GridRowUpdateModel} from "../../crud-grid/model/GridRowUpdateModel";
import {AxiosResponse} from "axios";
import {GridRowCreateModel} from "../../crud-grid/model/GridRowCreateModel";
import {DeviceStatus} from "../model/DeviceStatus";

const deviceListColumns: GridColDef[] = [
    {
        field: 'id',
        headerName: 'Id',
        width: 300,
        type: 'string',
        editable: false
    },
    {
        field: 'name',
        headerName: 'Name',
        width: 150,
        type: 'string',
        editable: true
    },
    {
        field: 'status',
        headerName: 'Status',
        width: 150,
        type: 'singleSelect',
        valueOptions: Object.values(DeviceStatus),
        editable: true
    },
    {
        field: 'maxConsumption',
        headerName: 'Maximum Consumption',
        width: 200,
        type: 'number',
        editable: true
    },
    {
        field: 'userId',
        headerName: 'User Id',
        width: 300,
        type: 'string',
        editable: true
    }
]

const defaultNewDevice: DeviceInfo = {
    id: '',
    name: '',
    status: DeviceStatus.OFFLINE,
    maxConsumption: 0,
    userId: ''
}

function DeviceList() {
    const [devices, setDevices] = useState<DeviceInfo[]>([]);

    useEffect(() => {
        getAllDevices()
            .then((response) => {
                setDevices(response.data);
            })
            .catch((error) => {
                console.log(error);
                throw error;
            })
    }, []);

    //todo make it work without as unknown as...
    return (
        <>
            <CrudGrid
                rows={devices}
                columns={deviceListColumns}
                updateFunc={(updateDevice as unknown) as (id: string, updateDto: GridRowUpdateModel) => Promise<AxiosResponse<GridValidRowModel>>}
                deleteFunc={deleteDevice}
                createFunc={(createDevice as unknown) as (creationDto: GridRowCreateModel) => Promise<AxiosResponse<GridValidRowModel>>}
                defaultNewRowFields={defaultNewDevice}
            />
        </>
    )
}

export default DeviceList;