import {DeviceStatus} from "./DeviceStatus";

/**
 * Device dto used in the request body of the update endpoint
 */
export default interface DeviceUpdate {
    name: string,
    status: DeviceStatus,
    maxConsumption: number,
    userId: string
}