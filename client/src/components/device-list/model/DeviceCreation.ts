import {DeviceStatus} from "./DeviceStatus";

export default interface DeviceCreation {
    name: string,
    status: DeviceStatus,
    maxConsumption: number,
    userId: string
}