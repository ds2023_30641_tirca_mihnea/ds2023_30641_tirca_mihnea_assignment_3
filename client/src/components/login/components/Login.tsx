import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Credentials from "../../../model/Credentials";
import {Form, NavigateFunction, useNavigate} from "react-router-dom";
import {useAppDispatch} from "../../../redux/slices/security/hooks";
import {useState} from "react";
import {authenticate} from "../../../redux/slices/security/securitySlice";
import {useIsLoggedIn} from "../../../redux/slices/security/selectors";
import {Path} from "../../../routing/model/path";

const defaultCredentials: Credentials = {
    username: '',
    password: ''
}

function Login() {
    const navigate: NavigateFunction = useNavigate();
    const isLoggedIn: boolean = useIsLoggedIn();

    if(isLoggedIn) {
        navigate(Path.HOME);
    }

    const dispatch = useAppDispatch();
    const [credentials, setCredentials] = useState<Credentials>(defaultCredentials);

    const handleUsernameChange = (event: any): void => {
        setCredentials({
            ...credentials,
            username: event.target.value as string
        });
    }

    const handlePasswordChange = (event: any): void => {
        setCredentials({
            ...credentials,
            password: event.target.value as string
        });
    }

    const handleSubmit = (event: any): void => {
        event.preventDefault();
        dispatch(authenticate(credentials));
        setCredentials(defaultCredentials);
    }

    return (
        <Container sx={{width: "30vw"}}>
            <Box
                sx={{
                    marginTop: "10vh",
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <Form method="post" onSubmit={handleSubmit}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        name="username"
                        autoComplete="username"
                        autoFocus
                        onChange={handleUsernameChange}
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={handlePasswordChange}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Sign In
                    </Button>
                </Form>
            </Box>
        </Container>
    );
}

export default Login;