import {AppBar, Divider, IconButton, List, ListItem, ListItemText, Stack} from '@mui/material'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import {Link} from "react-router-dom";
import {useAuthorization, useToken, useUsername} from "../../../redux/slices/security/selectors";
import {Path} from "../../../routing/model/path";
import {Authorization} from "../../../model/Authorization";
import ForumIcon from '@mui/icons-material/Forum';
import {useAppDispatch} from "../../../redux/slices/security/hooks";
import LogoutIcon from '@mui/icons-material/Logout';
import {logout} from "../../../redux/slices/security/securitySlice";
import {userManagementAxios} from "../../../axios/axios";
const renderHeaderElements = (authorization: Authorization | null) => {
    switch (authorization) {
        case Authorization.CLIENT:
            return (
                <List>
                    <ListItem component={Link} to={Path.MY_DEVICES}>
                        <ListItemText primary={"Devices"}/>
                    </ListItem>
                </List>
            );
        case Authorization.ADMIN:
            return (
                <List sx={{display: 'flex'}}>
                    <ListItem component={Link} to={Path.USER_LIST}>
                        <ListItemText primary={"Users"}/>
                    </ListItem>
                    <ListItem component={Link} to={Path.DEVICE_LIST}>
                        <ListItemText primary={"Devices"}/>
                    </ListItem>
                </List>
            );
        case null:
            return (<></>);
    }
}

function Header() {
    const authorization: Authorization | null = useAuthorization();
    const username: string | null = useUsername();
    const dispatch = useAppDispatch();
    userManagementAxios.defaults.headers.common['Authorization'] = useToken();

    return (
        <>
            <AppBar position="fixed">
                <Toolbar sx={{justifyContent: "space-between"}}>
                    <Stack direction="row" sx={{alignItems: 'center'}}>
                        <Typography variant="h6" color="white">
                            Energy Management System
                        </Typography>
                        <Divider orientation="vertical" flexItem sx={{margin: '10px'}} color='white'/>
                        {renderHeaderElements(authorization)}
                    </Stack>
                    <Stack direction="row">
                        <IconButton href={Path.CHATROOM}>
                            <ForumIcon/>
                        </IconButton>
                        <Typography>{username}</Typography>
                        <IconButton onClick={() => dispatch(logout())}>
                            <LogoutIcon/>
                        </IconButton>
                    </Stack>
                </Toolbar>
            </AppBar>
        </>
    )
}

export default Header