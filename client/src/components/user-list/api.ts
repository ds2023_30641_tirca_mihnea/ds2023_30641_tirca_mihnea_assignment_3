import {userManagementAxios} from "../../axios/axios";
import {USER_API} from "../../model/constants";
import UserInfo from "./model/UserInfo";
import UserUpdate from "./model/UserUpdate";

export const getAllUsers = async () => {
    return await userManagementAxios.get<UserInfo[]>(`${USER_API}`);
}

export const updateUser = async (userId: string, userUpdate: UserUpdate) => {
    return await userManagementAxios.put<UserInfo>(
        `${USER_API}/${userId}`,
        userUpdate
    );
}

export const deleteUser = async (userId: string) => {
    return await userManagementAxios.delete<void>(`${USER_API}/${userId}`);
}