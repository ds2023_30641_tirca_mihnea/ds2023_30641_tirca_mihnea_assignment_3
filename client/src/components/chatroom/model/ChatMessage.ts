export default interface ChatMessage {
    sender: string,
    receiver: string,
    text: string,
    timestamp: number
}
