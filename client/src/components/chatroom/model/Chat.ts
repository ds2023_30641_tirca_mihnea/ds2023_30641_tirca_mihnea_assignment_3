import ChatMessage from "./ChatMessage";

export default interface Chat {
    contact: string,
    messages: ChatMessage[]
}
