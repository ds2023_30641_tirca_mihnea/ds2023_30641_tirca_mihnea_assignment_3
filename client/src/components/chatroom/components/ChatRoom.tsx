import {useCallback, useEffect, useRef, useState} from "react";
import {Client, Message, over} from 'stompjs'
import SockJS from 'sockjs-client'
import {useUsername} from "../../../redux/slices/security/selectors";
import ChatMessage from "../model/ChatMessage";
import {IconButton, List, ListItem, ToggleButton, ToggleButtonGroup} from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import {getMyContacts} from "../api";
import Container from "@mui/material/Container";
import SendIcon from '@mui/icons-material/Send';
import Chat from "../model/Chat";
import {getMessages} from "../helper";

function ChatRoom() {
    const username = useUsername() as string;
    const [chats, setChats] = useState<Chat[]>([]);
    const [selectedUser, setSelectedUser] = useState<string | null>(null);
    const [text, setText] = useState<string>("");
    const stompClientRef = useRef<Client | null>(null)

    useEffect(() => {
        getMyContacts()
            .then(response => {
                setChats(response.data.map(contact => ({
                    contact: contact,
                    messages: []
                })))
            })
            .catch(error => {
                console.log(error);
                throw new Error();
            })
    }, []);

    /**
     * Adds message to the chat between this user and the contact
     */
    const addMessage = useCallback((contact: string, message: ChatMessage) => {
        const chatIndex = chats.findIndex(chat => chat.contact === contact);
        if (chatIndex !== -1) {
            const newChats = [...chats];
            newChats[chatIndex].messages.push(message);
            setChats(newChats);
        }
    }, [chats]);

    useEffect(() => {
        const onMessageReceived = (payload: Message) => {
            let messageReceived: ChatMessage = JSON.parse(payload.body);
            const sender = messageReceived.sender;
            addMessage(sender, messageReceived);
        }

        const onConnected = () => {
            stompClient!.subscribe(`/chat/${username}/private`, onMessageReceived)
        }

        const onError = (err: any) => {
            console.log(err)
        }

        let Sock = new SockJS('http://localhost:8080/ws');
        let stompClient = over(Sock);
        stompClient.connect({}, onConnected, onError);
        stompClientRef.current = stompClient;
    }, [username, addMessage]);

    /**
     * @return this user's contacts
     */
    const getContacts = (): string[] => {
        return chats.map(chat => chat.contact);
    }

    const sendMessage = (e: any) => {
        e.preventDefault();
        if (stompClientRef.current) {
            if (username && selectedUser) {
                let message: ChatMessage = {
                    sender: username,
                    receiver: selectedUser,
                    text: text,
                    timestamp: new Date().getTime()
                }
                addMessage(selectedUser, message);
                stompClientRef.current.send("/app/chat", {}, JSON.stringify(message));
                setText("");
            }
        }
    }

    return (
        <Container sx={{display: 'flex'}}>
            <ToggleButtonGroup
                sx={{border: 1}}
                value={selectedUser}
                exclusive
                orientation={"vertical"}
                onChange={(e, selected: string) => setSelectedUser(selected)}
            >
                {getContacts().map((username) => (
                    <ToggleButton
                        value={username}
                        // sx={{color: username === selectedUser ? 'success' : 'error'}}
                    >
                        {username}
                    </ToggleButton>
                ))}
            </ToggleButtonGroup>
            <Box sx={{height: 500, width: 500, border: 1}}>
                <List sx={{height: 400}}>
                    {selectedUser ? getMessages(chats, selectedUser).map(message => (
                        <ListItem>
                            {message.sender + ": " + message.text}
                        </ListItem>
                    )) : (
                        <></>
                    )}
                </List>
                <Box sx={{height: 100, display: 'flex', flexDirection: 'row'}}>
                    {selectedUser ? (
                        <form onSubmit={sendMessage}>
                            <TextField
                                value={text}
                                onChange={(e) => setText(e.target.value)}
                                sx={{flexGrow: 1}}
                            />
                            <IconButton type={"submit"}>
                                <SendIcon/>
                            </IconButton>
                        </form>
                    ) : (<></>)}
                </Box>
            </Box>
        </Container>
    )
}

export default ChatRoom;