import Chat from "./model/Chat";
import ChatMessage from "./model/ChatMessage";

/**
 * @return the messages from the chats given as a parameter with the contact given as a parameter
 */
export const getMessages = (chats: Chat[], contact: string): ChatMessage[] => {
    const chat = chats.find(chat => chat.contact === contact) as Chat;
    return chat.messages;
}
