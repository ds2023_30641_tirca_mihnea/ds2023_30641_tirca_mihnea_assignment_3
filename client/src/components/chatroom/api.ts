import {userManagementAxios} from "../../axios/axios";
import {CHAT_API} from "../../model/constants";
import Chat from "./model/Chat";

export const getMyChats = async () => {
    return await userManagementAxios.get<Chat[]>(`${CHAT_API}/chat`)
}

export const getMyContacts = async () => {
    return await userManagementAxios.get<string[]>(`${CHAT_API}/users`);
}