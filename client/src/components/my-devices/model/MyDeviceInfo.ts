import {DeviceStatus} from "../../device-list/model/DeviceStatus";

/**
 * Device information showed on the client's devices page
 */
export default interface MyDeviceInfo {
    name: string,
    status: DeviceStatus,
    maxConsumption: number
}