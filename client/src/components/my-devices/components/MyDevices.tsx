import {useEffect, useState} from "react";
import {getUserDevices} from "../api";
import MyDeviceInfo from "../model/MyDeviceInfo";
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";

function MyDevices() {
    const [devices, setDevices] = useState<MyDeviceInfo[]>([]);

    useEffect(() => {
        getUserDevices()
            .then(response => {
                setDevices(response.data);
            })
            .catch(error => {
                console.log(error);
                throw new Error();
            })
    }, []);

    //todo generify
    return (
        <>
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Status</TableCell>
                            <TableCell>Maximum Consumption</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {devices.map((device: MyDeviceInfo, index: number) => (
                            <TableRow key={index}>
                                <TableCell>{device.name}</TableCell>
                                <TableCell>{device.status}</TableCell>
                                <TableCell>{device.maxConsumption}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    )
}

export default MyDevices;