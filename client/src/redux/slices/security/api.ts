import {AxiosResponse} from "axios";
import {userManagementAxios} from "../../../axios/axios";
import {AUTHENTICATE_API, USER_API} from "../../../model/constants";
import Credentials from "../../../model/Credentials";
import UserData from "../../../model/UserData";

type JWTResponse = { access_token: string }
export const logIn = async (logInData: Credentials): Promise<string> => {
    const response: AxiosResponse<JWTResponse> = await userManagementAxios.post(AUTHENTICATE_API, logInData);
    return "Bearer " + response.data.access_token
}

export const getUserData = async (token: string): Promise<UserData> => {
    const response = await userManagementAxios.get<UserData>(
        `${USER_API}/data`,
        {headers: {'Authorization': token}}
    );
    return response.data;
}