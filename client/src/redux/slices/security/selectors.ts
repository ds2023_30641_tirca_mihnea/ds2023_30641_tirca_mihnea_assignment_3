import {useAppSelector} from "./hooks";
import {selectAuthorization, selectIsLoggedIn, selectToken, selectUserId, selectUsername} from "./securitySlice";

export const useUsername = () => {
    return useAppSelector(selectUsername);
}

export const useUserId = () => {
    return useAppSelector(selectUserId);
}

export const useToken = () => {
    return useAppSelector(selectToken);
}

export const useAuthorization = () => {
    return useAppSelector(selectAuthorization);
}

export const useIsLoggedIn = () => {
    return useAppSelector(selectIsLoggedIn);
}
