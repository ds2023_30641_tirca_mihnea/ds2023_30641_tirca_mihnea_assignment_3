import axios, {AxiosInstance} from "axios";

export const userManagementAxios: AxiosInstance = axios.create({
    baseURL: "http://localhost:8080"
})

export const deviceManagementAxios: AxiosInstance = axios.create({
    baseURL: "http://localhost:8081"
})