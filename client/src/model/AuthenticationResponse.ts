import {Authorization} from "./Authorization";

export default interface AuthenticationResponse {
    token: string,
    id: string;
    username: string;
    roles: Authorization[]
}