enum LoginStatus {
    LoggedOut = 'loggedOut',
    Pending = 'pending',
    LoggedIn = 'loggedIn'
}

export default LoginStatus;