import {Authorization} from "./Authorization";

export default interface UserData {
    readonly id: string,
    readonly username: string,
    readonly firstName: string,
    readonly lastName: string,
    readonly role: Authorization
}